﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace ListViewAPI.Model
{
    public class Logic

    {

        public async Task<string> GetJsonAsync()
        {
            var uri = new Uri("https://dog.ceo/api/breeds/image/random");
            HttpClient http = new HttpClient();
            var response = await http.GetAsync(uri);
            var result = " ";
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                string json = content.ToString();
                var jsonObj = JObject.Parse(json);
                var message = jsonObj["message"];
                result = message.ToString();
                Debug.WriteLine(message);

            }
            return result;


        }



    }
}
